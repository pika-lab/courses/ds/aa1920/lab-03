package sd.lab.concurrency.exercise;

import java.math.BigInteger;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

public interface AsyncCalculator {

    default CompletableFuture<BigInteger> factorial(long x) {
        return factorial(BigInteger.valueOf(x));
    }

    CompletableFuture<BigInteger> factorial(BigInteger valueOf);

    static AsyncCalculator newInstance(ExecutorService executorService) {
        throw new IllegalStateException("not implemented");
    }
}
